# README #

This project is a simple shooter game meant to be used to teach my AI students how to write FSMs using Unity's State Machine behaviors.  
It is also "over-commented" so that they can easily learn from the techniques.

### Version Info ###

* Version 1.0

### Contribution guidelines ###

* If you want to contribute, email me at mhenry@uat.edu

### Who do I talk to? ###

* Owned by Matthew "Hue" Henry - mhenry@uat.edu