﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_SpawnObject : AI_Behavior {

	public GameObject objectPrefab;
	public Vector3 position;
	public Vector3 rotation;
	public bool isLocalSpace; 

	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		base.OnStateEnter (animator, stateInfo, layerIndex);

		Vector3 newPosition = position;
		Quaternion newRotation;

		// If local, make it relative to ourselves, not to the world
		if (isLocalSpace) {
			newPosition += pawn.tf.position;
			newRotation = pawn.tf.rotation * Quaternion.Euler(rotation);
		} else
		{
			newRotation = Quaternion.Euler (rotation);
		}
		// Spawn it
		Instantiate(objectPrefab, newPosition, newRotation);

	}

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {

	}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {

	}


}
