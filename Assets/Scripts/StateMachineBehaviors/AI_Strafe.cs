﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Strafe : AI_Behavior {

	public enum Direction { Right, Left } ;
	public Direction direction;
	public float speedModifier = 1;

	// OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		base.OnStateEnter (animator, stateInfo, layerIndex);
	}

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		int directionModifier;
		if (direction == Direction.Right) {
			directionModifier = 1;
		} else {
			directionModifier = -1;
		}

		// Have the mover move the object, at "speed from data * our speed modifier) and use the directionModifier to move right or left (negative right).
		pawn.mover.Move (Vector3.right * (pawn.moveSpeed * speedModifier) * (directionModifier));
	}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	//override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
