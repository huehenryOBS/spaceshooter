﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_TargetRandomEnemy : AI_Behavior {

	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		base.OnStateEnter (animator, stateInfo, layerIndex);

		AIController cntl = pawn.controller as AIController;
		if (cntl != null) {

			// If i am the only AI left, I can't target anyone!
			if (GameManager.instance.AIs.Count <= 1) {
				return;
			}

			// Choose a random player
			int whichPlayer = Random.Range (0,GameManager.instance.AIs.Count);

			// If it's me, choose again!
			while (GameManager.instance.AIs[whichPlayer] == cntl) {
				whichPlayer = Random.Range (0,GameManager.instance.AIs.Count);
			}

			// Target that enemy, if it exists
			if (GameManager.instance.AIs [whichPlayer] != null) {
				cntl.currentTarget = GameManager.instance.AIs [whichPlayer].gameObject;
			}
		}

	}

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	//override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	//override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
