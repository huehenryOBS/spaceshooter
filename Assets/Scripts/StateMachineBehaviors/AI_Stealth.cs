﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Stealth : AI_Behavior {

	public float stealthAlpha = 0.2f;

	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		base.OnStateEnter (animator, stateInfo, layerIndex);

		//  Becomes "stealthy"
		// Get the sprite renderer(s) for this ship
		// For each renderer
		foreach (SpriteRenderer renderer in pawn.GetComponentsInChildren<SpriteRenderer>()) {
			// Change the tint so the alpha is changed
			renderer.color = new Color (renderer.color.r, renderer.color.g, renderer.color.b, stealthAlpha);
		}
	}

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {

	}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {

		// Get the sprite renderer(s) for this ship
		// For each renderer
		// Change the tint so the alpha is changed to 1 
		foreach (SpriteRenderer renderer in pawn.GetComponentsInChildren<SpriteRenderer>()) {
			// Change the tint so the alpha is changed
			renderer.color = new Color (renderer.color.r, renderer.color.g, renderer.color.b, 1.0f);
		}
	}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
