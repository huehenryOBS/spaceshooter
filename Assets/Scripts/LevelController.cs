﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour {

	// World Objects
	[Header("Level Data")]
	public List<GameObject> entityPrefabs;
	public int[] scores;

	[Header("World Locations")]
	public Transform[] spawnPoints;
	private int currentSpawnPoint = 0;

	void Awake () {
		// Start disabled
		gameObject.SetActive(false);

		// Initialize scores
		scores = new int[entityPrefabs.Count];
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		// If all entities are dead 
		if (GameManager.instance.players.Count + GameManager.instance.AIs.Count <= 1) {
			SpawnEntities ();
		}
	}


	public int NewSpawnPoint ( ) {
		int	newSpawn = Random.Range (0, spawnPoints.Length);
		while (newSpawn == currentSpawnPoint) {
			newSpawn = Random.Range (0, spawnPoints.Length);
		}
		return newSpawn;
	}

	void SpawnEntities () {
		// reset values
		GameManager.instance.ClearLists ();
		currentSpawnPoint = 0;

		// Spawn the AIs
		for (int i = 0; i < entityPrefabs.Count; i++) {
			currentSpawnPoint = NewSpawnPoint ();
			Instantiate (entityPrefabs[i], spawnPoints[currentSpawnPoint].position, spawnPoints[currentSpawnPoint].rotation);
		}
	}

	void OnEnable () {
		Debug.Log ("LEVEL START");

		// Make us the only level in the world
		if (GameManager.instance.level != this && GameManager.instance.level != null) {
			Destroy (GameManager.instance.level.gameObject);
		}
		GameManager.instance.level = this;

		// Destroy entities
		GameManager.instance.ClearLists ();

		// Spawn entities
		SpawnEntities();
	}

	void OnDisable() { 
		Debug.Log ("LEVEL DISABLE");

		if (GameManager.instance != null) {
			GameManager.instance.ClearLists ();
		}
	}
}
