﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour {

	public Transform tf;

	// Use this for initialization
	void Start () {
		tf = GetComponent<Transform> ();		
	}

	// Update is called once per frame
	void Update () {
		
	}

	public void Move (Vector3 moveVector, Space spaceToUse = Space.World) {
		// Move in the direction specified, either local or world space (If not passed a space, use world space)
		tf.Translate (moveVector * Time.deltaTime, spaceToUse);
	}

	public void MoveForward (float speed) {
		tf.Translate (tf.up * (speed * Time.deltaTime), Space.World);
	}

	public void MoveStrafe (float speed) {
		tf.Translate (tf.right * speed * Time.deltaTime, Space.World);
	}

	public void Turn (float turnSpeed) {
		tf.Rotate (0, 0, turnSpeed * Time.deltaTime);
	}


	public void TurnTowards (Vector3 lookPoint, float turnSpeed) {

		// Find the vector that points from our position to our target
		Vector3 vectorToTarget = lookPoint - tf.position;

		// Use the ArcTangent to find the radians that we would rotate (in the world) to look down our target vector
		float zRotation = Mathf.Atan2 (vectorToTarget.y, vectorToTarget.x);

		// Convert to degrees
		zRotation *= Mathf.Rad2Deg;

		// Subtract 90 degrees, so our top is pointing towards the object
		zRotation -= 90;

		// Find the quaternion that matches that euler rotation
		Quaternion quaternionToTarget = Quaternion.Euler (0f, 0f, zRotation);

		// Rotate towards that amount, but no faster than the "turnSpeed" variable we passed in per second.
		tf.rotation = Quaternion.RotateTowards (tf.rotation, quaternionToTarget, turnSpeed * Time.deltaTime);
	}

} 
