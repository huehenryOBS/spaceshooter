﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : Controller {

	[HideInInspector]  public Animator animator;
	public GameObject currentTarget;
	public float fieldOfViewAngle;
	public float sensoryPulse = 0.5f;

	private float countdownToSensePulse = 0;

	public override void Awake () {
		base.Awake ();
		animator = GetComponent<Animator> ();
	}

	// Use this for initialization
	public override void Start () {
		base.Start ();

		// Add to list
		GameManager.instance.AIs.Add(this);

		countdownToSensePulse = 0;
	}

	// Update is called once per frame
	public override void Update () {
		base.Update ();

		// Animator controls the AI
		// Just need to update all the data
		animator.SetFloat("x", pawn.tf.position.x);
		animator.SetFloat("y", pawn.tf.position.y);
		animator.SetFloat ("distanceFromSpawn", Vector3.Distance (pawn.tf.position, pawn.startPoint));

        // Set health
        if (pawn.health != null)
        {
            animator.SetFloat("health", pawn.health.currentHealth);
        }

        // Set object counts
        animator.SetInteger("AIsRemaining", GameManager.instance.AIs.Count);
        animator.SetInteger("playersRemaining", GameManager.instance.players.Count);
        animator.SetInteger("entitiesRemaining", GameManager.instance.AIs.Count + GameManager.instance.players.Count);

        // Set hasTarget bool
        animator.SetBool("hasTarget", currentTarget != null);

        // Pulse senses
        countdownToSensePulse -= Time.deltaTime;
        if (countdownToSensePulse <= 0) {

            // Only if we have a target 
            if (currentTarget != null)
            {
                // Check if in cone
                animator.SetBool("targetInCone", isObjectInCone(currentTarget));
                animator.SetBool("sphereCastToTarget", isObjectInSphereCast(currentTarget));
                //TODO: animator.SetBool ("targetInRange", isObjectInRange (currentTarget));

                // Set distance from Target
                animator.SetFloat("distanceFromTarget", Vector3.Distance(pawn.tf.position, currentTarget.transform.position));
            }

			// Reset pulse
			countdownToSensePulse = sensoryPulse;
		}

	}

	public override void OnDestroy() {
		base.OnDestroy ();

		// Remove from list
		GameManager.instance.AIs.Remove(this);	
	}

	public bool IsObjectInDistance (GameObject targetObject, float distance) {
		if (Vector3.Distance (targetObject.transform.position, pawn.tf.position) <= distance) {
			return true;
		} else {
			return false;
		}
	}

	public bool isObjectInCone (GameObject targetObject) {
		// If only passed target, use field of view angle and forward vector
		return isObjectInCone(targetObject, fieldOfViewAngle, pawn.tf.forward);
	}

	public bool isObjectInCone (GameObject targetObject, Vector3 direction) {
		// If only passed target and vector, assume field of view angle
		return isObjectInCone(targetObject, fieldOfViewAngle, direction);
	}

	public bool isObjectInCone (GameObject targetObject, float coneRadius) {
		// If only passed target and field of view angle, assume forward vector
		return isObjectInCone(targetObject, coneRadius, pawn.tf.forward);
	}

	public bool isObjectInCone (GameObject targetObject, float coneRadius, Vector3 directionVector) {
		// If no object is passed, return false
		if (targetObject == null)
			return false;

		// Find a vector pointing to the target
		Vector3 vectorToTarget = targetObject.transform.position - pawn.tf.position; 
		// Find the angle between the direction our cone is facing, and the direction to the target
		float angleToObject = Vector3.Angle (directionVector, vectorToTarget);
		// If that is less than our ConeRadius, we are in the cone!
		if (angleToObject <= coneRadius) {
			return true;
		}
		// Otherwise, we are outside the cone.
		return false;
	}

	public bool isObjectInSphereCast (GameObject targetObject, float radius = 1.0f) {
		// Spherecase forward with given radius, if they hit, then we can hit them 
		RaycastHit hitInfo;
		if (Physics.SphereCast(pawn.tf.position, radius, pawn.tf.forward, out hitInfo, Mathf.Infinity)) {
			if (hitInfo.collider.gameObject == targetObject) {
				return true;
			}
		}
		return false;
	}


}
