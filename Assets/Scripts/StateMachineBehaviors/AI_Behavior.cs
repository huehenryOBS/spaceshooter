﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Behavior : StateMachineBehaviour {

	[HideInInspector] protected Controller controller;
	[HideInInspector] protected Pawn pawn;

	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		controller = animator.GetComponent<Controller> ();
		pawn = controller.pawn;
	}

}
