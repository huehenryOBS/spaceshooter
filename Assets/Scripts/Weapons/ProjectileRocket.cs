﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileRocket : MonoBehaviour {

	public Controller shooter;
	public Controller target;
	[HideInInspector] public Mover mover;
	public float speed;
	public float lifespan;
	public float damageDone;

	public GameObject[] wreckage;

	// Use this for initialization
	void Start () {
		mover = GetComponent<Mover> ();
		Destroy (gameObject, lifespan);
	}
	
	// Update is called once per frame
	void Update () {

		// If no target
		if (target == null) {
			mover.MoveForward(speed);
		} 
		// If targeted
		else {
			// TODO: Use mover to look toward target and move forward
		}
	}

	public void OnTriggerEnter2D (Collider2D hitObject) {

		// Get the pawn we hit
		Pawn hitPawn = hitObject.gameObject.GetComponent<Pawn> ();

		// If we hit ourselves, or something that is not a pawn, do nothing
		if (hitPawn == shooter.pawn || hitPawn == null) {
			return;
		}

		// Check if we hit something that can take damage
		if (hitPawn.health != null) {
			// Tell it to take damage
			hitPawn.health.TakeDamage (gameObject, shooter, damageDone);

			// TODO: Play destruction sound

			// Destroy ourselves
			Destroy(gameObject);
		} else {
			// If what we hit can't take damage, just destroy ourselves
			Destroy(gameObject);
		}
	}

	public void OnDestroy () {

		// Generate all wreckage
		for (int i = 0; i < wreckage.Length; i++) {
			if (wreckage [i] != null) {
				Instantiate (wreckage [i]);
			}
		}

	}




}
