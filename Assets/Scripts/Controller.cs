﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour {

	[HideInInspector] public Pawn pawn;

	// Use this for initialization
	public virtual void Awake () {
	}

	public virtual void Start () {
		pawn = GetComponent<Pawn> ();
	}
	
	// Update is called once per frame
	public virtual void Update () {
		
	}

	public virtual void OnDestroy() {
	}
}
