﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestruct : MonoBehaviour {

	public float lifespan = 1.0f;

	// Use this for initialization
	void Start () {
		Destroy (gameObject, lifespan);
	}

}
