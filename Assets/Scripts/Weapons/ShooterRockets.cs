﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShooterRockets : ShooterBase {
	[Header("Prefabs")]
	public GameObject rocketPrefab;

	// Components

	// Use this for initialization
	public override void Start () {
		base.Start ();
	}
	
	// Update is called once per frame
	public override void Update () {
		base.Update ();
	}

	public override void Shoot (Vector2 shotDirection)
	{
		// Shoot rocket
		if (CanShoot ()) {
			// Process cooldown/ammo/baseclass
			base.Shoot (shotDirection);

			// If the rocket prefab is set 
			if (rocketPrefab != null) {
				// TODO: Play shoot sound

				// Generate the rocket
				GameObject newRocket = Instantiate (rocketPrefab, shotTf.position, shotTf.rotation) as GameObject;

				//float angleToTurn = Vector3.Angle (shotTf.up, shotTf.TransformDirection((Vector3)shotDirection));
				//newRocket.transform.Rotate (new Vector3(0,0,angleToTurn));

				ProjectileRocket rocketScript = newRocket.GetComponent<ProjectileRocket> ();
				rocketScript.damageDone = damageDone;
				rocketScript.shooter = owner.controller;
			}
		} else {
			//TODO: Play rocket fail sound

		}

	}
}
