﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour {

	public float maxHealth;
	public float currentHealth;
	public AudioClip deathSound;
	public AudioClip hitSound;
	public GameObject[] wreckage;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void TakeDamage (GameObject source, Controller instigator, float amount) {

		// Subtract health
		currentHealth -= amount;

		// Hit sound
		if (hitSound != null) {
			AudioSource.PlayClipAtPoint (hitSound, transform.position);
		}

		// Chech for death
		if (currentHealth <= 0) {
			Die (instigator);
		}
	}

	public void Die (Controller instigator) {

		// If something killed us
		if (instigator != null) {
			// if it is a player that killed us
			PlayerController pk = instigator.GetComponent<PlayerController> ();
			if (pk != null) {
				// TODO: Add to score
		
			}
		}

		// Generate all wreckage
		for (int i = 0; i < wreckage.Length; i++) {
			if (wreckage [i] != null) {
				Instantiate (wreckage [i], transform.position, transform.rotation);
			}
		}

		// Play sound
		if (deathSound != null) {
			AudioSource.PlayClipAtPoint (deathSound, transform.position);
		}

		// Queue for destruction
		Destroy (gameObject);
	}
}
