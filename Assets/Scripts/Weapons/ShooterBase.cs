﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ShooterBase : MonoBehaviour {

	// Public data
	[Header("Common Weapon Data")]
	public float cooldown;
	public float damageDone;
	public float maxAmmo = Mathf.Infinity;
	public float ammoRemaining = Mathf.Infinity;
	public Transform shotTf;
	public AudioClip sound;

	// Private vars
	private float cooldownTimeRemaining;
	protected Pawn owner;

	// Use this for initialization
	public virtual void Start () {
		owner = GetComponent<Pawn> ();

		if (shotTf == null) {
			shotTf = GetComponent<Transform> ();
		}
	}

	// Update is called once per frame
	public virtual void Update () {
		UpdateCooldownTimer ();
	}

	public bool CanShoot () {
		// True if the cooldown is up and we have ammo (>0 or infinite!)
		if (cooldownTimeRemaining <= 0.0f && ( ammoRemaining == Mathf.Infinity || ammoRemaining > 0.0f) )
			return true;

		// otherwise false
		return false;
	}


	public virtual void Shoot (Vector2 direction) {

		// if not infintite ammo, reduce by one
		if (ammoRemaining != Mathf.Infinity) {
			ammoRemaining -= 1.0f;
		}

		// Actual Shooting is handled in child class
		cooldownTimeRemaining = cooldown;

		// Play sound
		if (sound != null) {
			AudioSource.PlayClipAtPoint (sound, shotTf.position);
		}
	}


	void UpdateCooldownTimer () {
		if (cooldownTimeRemaining >= 0) {
			cooldownTimeRemaining -= Time.deltaTime;
		}
	}
}
