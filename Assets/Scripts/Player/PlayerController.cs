﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Controller {

	[Header("Input Controls")]
	public KeyCode moveUpKey = KeyCode.W;
	public KeyCode moveDownKey = KeyCode.S;
	public KeyCode turnRightKey = KeyCode.R;
	public KeyCode turnLeftKey = KeyCode.Q;
	public KeyCode strafeRightKey = KeyCode.D;
	public KeyCode strafeLeftKey = KeyCode.A;
	public KeyCode primaryWeaponKey = KeyCode.Mouse0;
	public KeyCode secondaryWeaponKey = KeyCode.Mouse1;
	public KeyCode nextWeapon;
	public KeyCode prevWeapon;

	[Header("Player Data")]
	public int score;
	public int lives;



	// Use this for initialization
	public override void Start () {
		base.Start ();

		// Add to list
		GameManager.instance.players.Add(this);	
	}
	
	// Update is called once per frame
	public override void Update () {
		base.Update ();

		if (Input.GetKey (moveUpKey)) {
			pawn.mover.Move (pawn.tf.up * pawn.moveSpeed);
		}
		if (Input.GetKey (moveDownKey)) {
			pawn.mover.Move (-pawn.tf.up * pawn.moveSpeed);
		}
		if (Input.GetKey (strafeRightKey)) {
			pawn.mover.Move (pawn.tf.right * pawn.strafeSpeed);
		}
		if (Input.GetKey (strafeLeftKey)) {
			pawn.mover.Move (-pawn.tf.right * pawn.strafeSpeed);
		}
		if (Input.GetKey (primaryWeaponKey)) {
			pawn.shooter.Shoot ((Vector2)pawn.tf.up);
		}
		if (Input.GetKey (turnRightKey)) {
			pawn.mover.Turn (-pawn.turnSpeed);
		}
		if (Input.GetKey (turnLeftKey)) {
			pawn.mover.Turn (pawn.turnSpeed);
		}


	}

	public override void OnDestroy() {
		base.OnDestroy ();

		// Remove from list
		GameManager.instance.players.Remove(this);	
	}
}
