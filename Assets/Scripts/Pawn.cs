﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pawn : MonoBehaviour {

	[Header("Data")]
	public float moveSpeed;
	public float turnSpeed;
	public float strafeSpeed;
	public Vector3 startPoint;

	[Header("Components")]
	public Mover mover;
	public ShooterBase shooter;
	public Health health;
	public Transform tf; 
	public Controller controller;

	// Use this for initialization
	void Start () {
		mover = GetComponent<Mover> ();
		shooter = GetComponent<ShooterBase>();
		tf = GetComponent<Transform> ();
		health = GetComponent<Health> ();

		// Unless our controller was set by other methods, grab the controller off this object
		if (controller == null)
			controller = GetComponent<Controller> ();

		// Save our start point
		startPoint = tf.position;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
