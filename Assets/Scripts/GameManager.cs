﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

	// Singleton
	public static GameManager instance;

	// Objects
	public List<PlayerController> players;
	public List<AIController> AIs;
	public LevelController level;
	public prefabList prefabs;

	// Data
	public float maxLives;

	void Awake () {
		// Create Game Manager singleton
		if (instance == null) {
			instance = this;
			DontDestroyOnLoad (gameObject);
		} else {
			Destroy (gameObject);
		}
	}

	// Use this for initialization
	void Start () {

		
	}

	public void Reset() {
		ClearLists ();
	}

	// Update is called once per frame
	void Update () {
		
	}

	public void ClearLists () {
		foreach (PlayerController obj in players) {
			Destroy (obj.gameObject);
		}
		foreach (AIController obj in AIs) {
			Destroy (obj.gameObject);
		}
	}
}


[System.Serializable] 
public class prefabList {

	public GameObject playerPrefab;
	public List<GameObject> AIPrefabs;

}

