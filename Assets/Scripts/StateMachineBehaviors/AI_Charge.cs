﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Charge : AI_Behavior {

	public GameObject particlePrefab;
	public float distance = 0.1f;
	private GameObject spark;

	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		base.OnStateEnter (animator, stateInfo, layerIndex);

		spark = Instantiate (particlePrefab, pawn.tf.position, Quaternion.identity) as GameObject;
		spark.transform.parent = pawn.tf.transform;
	}

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		Vector2 newPosition = Random.insideUnitCircle * (distance * Time.deltaTime);
		pawn.tf.position = pawn.tf.position + (Vector3)newPosition;
	}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		Destroy (spark);
	}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
